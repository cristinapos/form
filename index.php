

<?php
include 'image.php';
include 'function.php';
include 'connect.php';
mysqli_close($conn);
?>

<script src="javascript/index.js"></script>
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Form</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <section class="container_section border">
            <h1 class="title">Register form</h1>
            <div class="formular">
                <form method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1e">First name:</label><br/>
                        <input  class="form-control-sm form-control" id="exampleInputEmail1" type="text" name="firstName" required aria-describedby="fistName" placeholder="Enter your first name"/>
                        <small id="firstName" class="form-text text-muted">
                            We'll never share your email with anyone else.
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1e">Last name:</label><br/>
                        <input class="form-control-sm form-control" type="text" id="exampleInputEmail1" name="lastName" required aria-describedby="lastName" placeholder="Enter your last name"/>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1e">User name:</label><br/>
                        <input class="form-control-sm form-control" type="text" id="exampleInputEmail1" name="userName" required aria-describedby="userName" placeholder="Enter your user name"/>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Gender:</label>
                        <select name="gender" class="form-control-sm form-control" id="exampleFormControlSelect1">
                            <option  class="select" value="select">Select</option>
                            <option  class="select" value="male">Male</option>
                            <option  class="select" value="female">Female</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1e">Phone: </label><br/>
                        <input class="form-control-sm form-control" type="text" id="exampleInputEmail1" name="phone"  required aria-describedby="phone" placeholder="Enter your phone number" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1e">Address:</label><br/>
                        <input class="form-control-sm form-control" type="text" id="exampleInputEmail1" name="address"  required aria-describedby="address" placeholder="Enter your address" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1e">City:</label><br/>
                        <input class="form-control-sm form-control" type="text" id="exampleInputEmail1" name="city"  required aria-describedby="city" placeholder="Enter your city"/>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1e">Country:</label><br/>
                        <input class="form-control-sm form-control" type="text" id="exampleInputEmail1" name="country"  required aria-describedby="country" placeholder="Enter your country"/>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlFile1">Chose a profile picture:</label>
                        <input type="file" class="form-control-file-sm form-control-file" id="exampleFormControlFile1" name="image" accept="image/png, image/jpeg" />
                    </div>
                    <div class="buttons">
                        <button class="button_one btn btn-primary" type="sumbit" value="submit" name="save" onclick="on_submit()">
                            Submit
                        </button>
                    </div>
                </form>
                <div class="message">
                    <?php
                        echo isset($message) ? $message : '';
                    ?>
                </div>
            </div>
        </section>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>