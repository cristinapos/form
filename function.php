<?php

if (isset($_POST['save']) && $_POST['save'] == 'submit') {
    if (file_exists('file.json')) {
        $final_data = fileWriteAppend();
        if (file_put_contents('file.json', $final_data)) {
            $message = "<p class='text-success'>You have successfully registered</p>";
        }
    } else {
        $final_data = fileCreateWrite();
        if (file_put_contents('file.json', $final_data)) {
            $message = "<p class='text-success'>You have successfully registered</p>";
        }
    }
}

function fileWriteAppend() : string
{
    $current_data = file_get_contents('file.json');
    $array_data = json_decode($current_data, true);
    $extra = array(
        'first_name' => $_POST['firstName'],
        'last_name' => $_POST['lastName'],
        'user_name' => $_POST['userName'],
        'gender' => $_POST['gender'],
        'phone' => $_POST['phone'],
        'address' => $_POST['address'],
        'city' => $_POST['city'],
        'country' => $_POST['country'],
    );
    $array_data[] = $extra;
    $final_data = json_encode($array_data);

    return $final_data;
}

function fileCreateWrite() : string
{
    $file = fopen("file.json", "w");
    $array_data = array();
    $extra = array(
        'first_name' => $_POST['firstName'],
        'last_name' => $_POST['lastName'],
        'user_name' => $_POST['userName'],
        'gender' => $_POST['gender'],
        'phone' => $_POST['phone'],
        'address' => $_POST['address'],
        'city' => $_POST['city'],
        'country' => $_POST['country'],
    );
    $array_data[] = $extra;
    $final_data = json_encode($array_data);
    fclose($file);

    return $final_data;
}

function debug($data) : void
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';

    return;
}
